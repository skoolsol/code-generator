<template>
    <div class="app-container">
        <div class="filter-container">
            <!-- 查询条件 -->
            <el-input
                    v-model="listQuery.${secondModuleName}Code"
                    @keyup.enter.native="handleFilter"
                    :placeholder="$t('${secondModuleName}.label.${secondModuleName}Code')"
                    class="filter-item"
                    style="width: 200px"
            ></el-input>
            <!-- 查询 -->
            <el-button class="filter-item" type="primary" @click="handleFilter">{{ $t('common.button.query') }}
            </el-button>
            <!-- 重置 -->
            <el-button class="filter-item" type="primary" @click="handleTopReset">{{ $t('common.button.reset') }}
            </el-button>
            <!-- 按钮栏 -->
            <div>
                <!-- 表格操作按钮 -->
                <!-- 列设置 -->
                <el-button class="filter-item" type="primary" @click="columnsSettings">{{
                    $t('common.button.columnsSettings')
                    }}
                </el-button>
                <!-- 重置列设置 -->
                <el-button class="filter-item" type="primary" @click="resetColumnSettings">{{
                    $t('common.button.resetColumnSettings')
                    }}
                </el-button>
                <!-- 导出全部 -->
                <el-button class="filter-item" type="primary" @click="exportExcel">{{
                    $t('common.button.exportExcel')
                    }}
                </el-button>
                <!-- 导出当前页 -->
                <el-button class="filter-item" type="primary" @click="exportCurrentPage">{{
                    $t('common.button.exportCurrentPage')
                    }}
                </el-button>
                <!-- 功能按钮 -->
                <span style="font-size: 25px; color: #909399;">&nbsp;|&nbsp;</span>
                <!-- 任务执行 -->
                <!-- 新增 -->
                <el-button class="filter-item" v-if="${secondModuleName}Manager_btn_add" type="primary"
                           @click="handleAdd">{{
                    $t('common.button.add')
                    }}
                </el-button>
                <!-- 修改 -->
                <el-button class="filter-item" v-if="${secondModuleName}Manager_btn_update" type="primary"
                           @click="handleEdit">{{
                    $t('common.button.modify')
                    }}
                </el-button>
                <!-- 查看 -->
                <el-button class="filter-item" v-if="${secondModuleName}Manager_btn_view" type="primary"
                           @click="handleView">{{
                    $t('common.button.view')
                    }}
                </el-button>
                <!-- 生效 -->
                <el-button class="filter-item" v-if="${secondModuleName}Manager_btn_active" type="primary"
                           @click="handleActive">{{
                    $t('common.button.active')
                    }}
                </el-button>
                <!-- 失效 -->
                <el-button class="filter-item" v-if="${secondModuleName}Manager_btn_unActive" type="primary"
                           @click="handleUnActive">{{
                    $t('common.button.unActive')
                    }}
                </el-button>
            </div>
        </div>
        <sv-table
                ref="SvTable"
                :api="page"
                :data.sync="tableInfo.data"
                :table-index="false"
                :field-list="tableInfo.fieldList"
                :query="listQuery"
                pk="${secondModuleName}Id"
                :refresh="tableInfo.refresh"
                :single-select="true"
                @selection-change="selectionChange"
                :max-export-rows="20000"
                :export-file-name="$t('${secondModuleName}.label.exportFileName')"
                module="${secondModuleName}"
        ></sv-table>
    </div>
</template>

<script>
    import {page} from '@/api/${moduleName}/${secondModuleName}/index';
    import {mapGetters} from 'vuex';

    export default {
        name: '${secondModuleName}',
        data() {
            return {
                listQuery: {
                        ${secondModuleName}Code: undefined
                },
                page,
                tableInfo: {
                    data: [],
                    fieldList: [
                        #foreach($column in $columns)
                            #if($column.columnName != $pk.columnName)
                                // ${column.comments}
                                {
                                    prop: '${column.attrname}',
                                    label: this.$t('${secondModuleName}.label.${column.attrname}'),
                                    width: 140,
                                },
                            #end
                        #end
                    ],
                    refresh: 1
                },
                currentRow: undefined,
                // 权限
                    ${secondModuleName}Manager_btn_add: false,
                    ${secondModuleName}Manager_btn_update: false,
                    ${secondModuleName}Manager_btn_view: false,
                    ${secondModuleName}Manager_btn_active: false,
                    ${secondModuleName}Manager_btn_unActive: false
            }
        },
        activated() {
            this.getList()
        },
        computed: {
            ...mapGetters(['elements'])
        },
        created() {
            this.${secondModuleName}Manager_btn_add = this.elements['${secondModuleName}Manager:btn_add']
            this.${secondModuleName}Manager_btn_update = this.elements['${secondModuleName}Manager:btn_update']
            this.${secondModuleName}Manager_btn_view = this.elements['${secondModuleName}Manager:btn_view']
            this.${secondModuleName}Manager_btn_active = this.elements['${secondModuleName}Manager:btn_active']
            this.${secondModuleName}Manager_btn_unActive = this.elements['${secondModuleName}Manager:btn_unActive']
        },
        methods: {
            // 刷新sv-table
            getList() {
                this.tableInfo.refresh = Math.random()
                this.currentRow = undefined
            },
            // 查询
            handleFilter() {
                this.getList()
            },
            // 查询重置
            handleTopReset() {
                this.listQuery = {
                        ${secondModuleName}Code: undefined
                }
                this.getList()
            },
            // 打开列设置
            columnsSettings() {
                this.$refs.SvTable.columnsSettings()
            },
            // 重置列设置
            resetColumnSettings() {
                this.$refs.SvTable.resetColumnSettings()
            },

            // 导出全部数据
            exportExcel() {
                this.$refs.SvTable.exportExcel()
            },
            // 导出当前页数据
            exportCurrentPage() {
                this.$refs.SvTable.exportCurrentPage()
            },
            // 新增
            handleAdd() {
                this.$openTag(this, {
                    name: '${secondModuleName}',
                    title: this.$t('${secondModuleName}.title.add'),
                    status: 'create'
                })
            },
            // 修改
            handleEdit() {
                if (!this.currentRow) {
                    // 请选择一条数据进行操作的提示信息
                    this.$notify({
                        title: this.$t('common.title.indicate'),
                        message: this.$t('common.message.onDataToOperationEdit'),
                        type: 'warning',
                        duration: 2000
                    })
                } else {
                    this.$openTag(this, {
                        name: '${secondModuleName}',
                        title: this.$t('${secondModuleName}.title.modify'),
                        status: 'update',
                        id: this.currentRow.${secondModuleName}Id
                    })
                }
            },
            // 查看
            handleView() {
                if (this.currentRow === undefined) {
                    // 请选择一条数据进行操作的提示信息
                    this.$notify({
                        title: this.$t('common.title.indicate'),
                        message: this.$t('common.message.onDataToOperationView'),
                        type: 'warning',
                        duration: 2000
                    })
                } else {
                    this.$openTag(this, {
                        name: '${secondModuleName}',
                        title: this.$t('${secondModuleName}.title.view'),
                        status: 'view',
                        id: this.currentRow.${secondModuleName}Id
                    })
                }
            },
            // 生效
            async handleActive() {
                if (!this.currentRow) {
                    // 请选择一条数据进行操作的提示信息
                    this.$notify({
                        title: this.$t('common.title.indicate'),
                        message: this.$t('common.message.onDataToOperationActive'),
                        type: 'warning',
                        duration: 2000
                    })
                    return
                }
                if (this.currentRow.isActive === '1') {
                    this.$notify({
                        title: this.$t('common.title.indicate'),
                        message: this.$t('${secondModuleName}.message.hasActive'),
                        type: 'warning',
                        duration: 2000
                    })
                    return
                }
                // 调用生效api
            }
            ,
            // 失效
            async handleUnActive() {
                if (!this.currentRow) {
                    // 请选择一条数据进行操作的提示信息
                    this.$notify({
                        title: this.$t('common.title.indicate'),
                        message: this.$t('common.message.onDataToOperationUnActive'),
                        type: 'warning',
                        duration: 2000
                    })
                    return
                }
                if (this.currentRow.isActive === '0') {
                    this.$notify({
                        title: this.$t('common.title.indicate'),
                        message: this.$t('${secondModuleName}.message.hasUnActive'),
                        type: 'warning',
                        duration: 2000
                    })
                    return
                }
                // 调用失效api
            },
            // 获取选中行的数据
            selectionChange(row) {
                this.currentRow = row
            }
        }
    }
</script>
