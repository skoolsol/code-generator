package ${package}.rest;

import com.savor.security.auth.client.annotation.CheckClientToken;
import com.savor.security.auth.client.annotation.CheckUserToken;
import com.savor.security.common.rest.BaseController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ${package}.biz.${className}Biz;
import ${package}.entity.${className};

import java.util.Map;

/**
 * ${comments}视图层
 *
 * @author ${author}
 * @date ${datetime}
 */
@RestController
@RequestMapping("${secondModuleName}" )
@CheckClientToken
@CheckUserToken
public class ${className}Controller extends BaseController<${className}Biz,${className}> {
    /**
     *  分页
     * @date  ${datetime}
     * @param params
     */
    @Override
    @GetMapping("page" )
    public TableResultResponse<${className}> list(@RequestParam Map<String, Object> params) {
        PageQuery<Map<String, Object>> query = new PageQuery<>(params, params);
        Page<${className}> result = PageMethod.startPage(query.getPage(), query.getLimit());
        baseBiz.selectListQuery(params);
        return new TableResultResponse<>(result.getTotal(), result.getResult());
    }

    /**
     * 保存
     * @date   ${datetime}
     * @param dto
     */
    @PostMapping("save" )
    public ObjectRestResponse<${className}DTO> save(@RequestBody ${className}DTO dto) {
        baseBiz.save${className}(dto);
        return new ObjectRestResponse<>();
    }


    /**
     * 根据ID获取数据
     * @date   ${datetime}
     * @param id
     */
    @GetMapping("getQtcSzzById/" )
    public ObjectRestResponse<${className}DTO> get${className}ById(@PathVariable(value = "id" ) String id) {
        ObjectRestResponse<${className}DTO> reponse = new ObjectRestResponse<>();
        reponse.setData(this.baseBiz.get${className}ById(id));
        return reponse;
    }

    /**
     * 失效
     * @date   ${datetime}
     * @param dto
     */
    @PostMapping("unActive" )
    public ObjectRestResponse<${className}DTO> unActive${className}(@RequestBody ${className}DTO dto) {
        this.baseBiz.unActive${className}(dto);
        return new ObjectRestResponse<>();
    }

    /**
     *  生效
     * @date   ${datetime}
     * @param dto
     */
    @PostMapping("active" )
    public ObjectRestResponse<${className}DTO> active${className}(@RequestBody ${className}DTO dto) {
        this.baseBiz.active${className}(dto);
        return new ObjectRestResponse<>();
    }
}