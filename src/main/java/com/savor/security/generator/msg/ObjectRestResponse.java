/*
 *  Copyright (C) 2018 Savor

 *  Savor-Enterprise 企业版源码
 *  郑重声明:
 *  如果你从其他途径获取到，请告知Savor。
 *  Savor将追究授予人和传播人的法律责任!

 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.

 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.

 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package com.savor.security.generator.msg;

import java.util.MissingFormatArgumentException;

/**
 * Created by savor on 2017/6/11.
 */
public class ObjectRestResponse<T> extends BaseResponse {
    private T data;

    public static <T> ObjectRestResponse success() {
        return new ObjectRestResponse<T>();
    }

    public static <T> ObjectRestResponse success(T data) {
        return new ObjectRestResponse<T>().data(data);
    }

    public static <T> ObjectRestResponse failed() {
        return new ObjectRestResponse<T>().status(BaseResponse.SERVER_FAILED_STATUS);
    }

    public static <T> ObjectRestResponse failed(String message) {
        ObjectRestResponse<T> response = new ObjectRestResponse<T>();
        return response.status(BaseResponse.SERVER_FAILED_STATUS).message(message);
    }

    public ObjectRestResponse message(String message) {
        this.message = message;
        return this;
    }

    public ObjectRestResponse message(String message, Object... objs) {
        try {
            this.message = String.format(message.replaceAll("\\{\\}", "%s"), objs);
        } catch (MissingFormatArgumentException formatException) {
            throw new RuntimeException("设置message失败，占位符与参数个数不匹配");
        }
        return this;
    }

    public ObjectRestResponse status(int status) {
        this.status = status;
        return this;
    }

    public ObjectRestResponse data(T data) {
        this.setData(data);
        return this;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
